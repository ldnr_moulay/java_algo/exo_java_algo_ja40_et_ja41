/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exo_java_algo_classes;

/**
 *
 * @author stag
 */
public class Voiture {

    public String couleur;
    public String marque;
    public String modele;
    public int compteur = 0;
    public boolean demarre = false;
    

    public void demarrer() {
        demarre = true;
    }

    public void avancer(int km) {
        compteur += km;
    }

    public void arreter() {
        demarre = false;
    }

    public void afficher() {
        System.out.println("-------Infos sur la voiture-------");
        System.out.println("Couleur =" + couleur);
        System.out.println("Marque =" + marque);
        System.out.println("Modele =" + modele);
        System.out.println("compteur =" + compteur);
        System.out.println("Moteur demarré ? =" + demarre);
        
    }

}
